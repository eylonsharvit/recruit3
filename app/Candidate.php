<?php

namespace App;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use Sortable;
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     

}

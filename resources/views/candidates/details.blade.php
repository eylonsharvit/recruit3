
@extends('layouts.app')

@section('title', 'Candidates')

@section('content')
<td>{{$candidate->id}}</td>
<td>{{$candidate->name}}</td>
<td>{{$candidate->email}}</td>
<td>{{$candidate->age}}</td>



<div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id))    
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id))
                           {{$candidate->status->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$candidate->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                         <a class="dropdown-item" href="{{route('candidates.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
           
                
@endsection